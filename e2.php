<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Tabla</title>
  <style>
    body {color: #000;}
    .main-title {background-color: yellow}
    thead {background-color: gray}
    tr {background-color: white}
    tr:nth-child(even) {background-color: green}
  </style>
</head>
<body>
  <?php
    class Beverage {
      private $name; 
      private $q;
      private $price;

      public __construct($n, $quant, $p) {
        $this->name = $n;
        $this->q = $quant;
        $this->price = $p;
      }

      public setName($n) {
        $this->name = $n;
      }
      public setQ($quant) {
        $this->q = $quant;
      }
      public setPrice($p) {
        $this->price = $p;
      }

      public getName() {
        return $this->name;
      }
      public getQ() {
        return $this->q;
      }
      public getPrice() {
        return $this->price;
      }
    }

    $bevs = array(
      new Beverage("Coca Cola", 100, 4500),
      new Beverage("Pepsi", 30, 4800),
      new Beverage("Sprite", 20, 4500),
      new Beverage("Guaraná", 200, 4500),
      new Beverage("SevenUp", 24, 4800),
      new Beverage("Mirinda Naranja", 56, 4800),
      new Beverage("Mirinda Guaraná", 89, 4800),
      new Beverage("Fanta Naranja", 10, 4500),
      new Beverage("Fanta Piña", 2, 4500)
    )
  ?>
  <table>
    <thead>
      <tr>
        <th class="main-title" colspan="3">Productos</th>
      </tr>
    </thead>
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Cantidad</th>
        <th>Precio(Gs)</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($bevs as &$bev) { ?>
        <tr>
          <td><?php echo $bev->getName() ?></td>
          <td><?php echo $bev->getQ() ?></td>
          <td><?php echo $bev->getPrice() ?></td>
        </tr>
      <?php } ?>
    </tbody>
  </table>

</body>
</html>